# demo-zipkin
分布式调用链使用范例

# 分布式调用链
简述：
[Google Dapper](http://bigbully.github.io/Dapper-translation/)

研发环境体验地址
[zipkin](http://192.168.96.149:9411/zipkin/)

## 采用springboot+brave+zipkin实现

### 使用说明

1. 配置maven地址
```
<distributionManagement>
    <repository>
        <id>releases</id>
        <url>http://192.168.96.23:8081/nexus/content/repositories/releases</url>
    </repository>
    <snapshotRepository>
        <id>snapshots</id>
        <url>http://192.168.96.23:8081/nexus/content/repositories/snapshots</url>
    </snapshotRepository>
</distributionManagement>
```
2. 引入maven依赖common
```
    <dependency>
        <groupId>com.km</groupId>
        <artifactId>brave-spring-boot-starter</artifactId>
        <version>1.0-SNAPSHOT</version>
    </dependency>
```
3. yml配置zipkin参数
```
zipkin:
  enabled: true
  reporterType: kafka
  host: 192.168.97.211 # kafka地址
  port: 9092 # 端口
  serviceName: service-haha
```
4. 加入拦截器
```
import com.km.tracing.ZipkinWebConfiguration;
import org.springframework.context.annotation.Configuration;
@Configuration
public class WebConfig extends ZipkinWebConfiguration {
}
```
或者
```
@ComponentScan({"你的springboot启动目录","com.km"})
```
如果你的springboot默认目录与com.km一致则不需要额外配置

### 日志记录traceId

之前我们定义了一套[日志格式](http://wiki.ktvme.cn/index.php?title=JSON%E6%A0%BC%E5%BC%8F%E6%97%A5%E5%BF%97)，里面的request_id可以设置为traceId，用来查询调用链的日志。

目前记录日志，要么使用Filter，要么使用Interceptor，在Interceptor中可以直接通过`MDC.get("traceId")`获取traceId，由于traceId是在Interceptor中生成，所以想要在Filter中使用需要做对应关系的处理，要么采用中间变量保存，要么在response返回值中设置，这是目前所能想到的方式，如果你有更好的方式，可以提出来。
获取traceId

```
String traceId = MDC.get("traceId")
```

### Http请求

由于每个人所使用的http请求库，有所区别，例如RestTemplate、OkHttpClient、HttpClient。
参考第3点brave/instrumentation中的实现。用来生成cs、cr标记。

### demo地址

[demo-zipkin](https://gitee.com/zhengzy/demo-zipkin.git)

### 参考

1. [opentracing](https://wu-sheng.gitbooks.io/opentracing-io/content/pages/quick-start.html)
2. [zipkin](https://github.com/openzipkin/zipkin)
3. [brave](https://github.com/openzipkin/brave)
4. [opentracing-contrib](https://github.com/opentracing-contrib)
5. [zipkin-trace](https://github.com/merapar/zipkin-trace)
6. [zipkin-reporter-java](https://github.com/openzipkin/zipkin-reporter-java)